﻿CREATE PROCEDURE [dbo].[spStoreXml]
	@xml xml,
	@xmlMd5 nvarchar(32)
AS
BEGIN
	SET NOCOUNT ON;

	declare @inserted bit = 0;

	if not exists(select 1 from TableWithXmlField where md5 = @xmlMd5)
	begin
		insert into TableWithXmlField (XmlField, md5) values (@xml, @xmlMd5);
		set @inserted = 1;
	end

	select @inserted;

END
