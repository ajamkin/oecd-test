﻿CREATE TABLE [dbo].[TableWithXmlField] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [XmlField] XML           NOT NULL,
    [md5]      NVARCHAR (32) NOT NULL,
    CONSTRAINT [PK_TableWithXmlField] PRIMARY KEY CLUSTERED ([Id] ASC)
);

