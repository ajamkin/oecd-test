using System.Security.Cryptography;
using System.Text;

namespace oecd_test
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public partial class OecdTestContext : DbContext
    {
        public OecdTestContext()
            : base("name=OecdTestContext")
        {
        }

        public virtual DbSet<TableWithXmlField> TableWithXmlFields { get; set; }

        public bool spStoreXml(string xml)
        {
            using (var md5 = MD5.Create())
            {
                var hash = BitConverter.ToString(md5.ComputeHash(Encoding.Unicode.GetBytes(xml))).Replace("-", "");
                return Database.SqlQuery<bool>("spStoreXml {0}, {1}", xml, hash).FirstOrDefault();
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
