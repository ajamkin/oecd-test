﻿using System;
using System.Linq;

namespace oecd_test
{
    public class OecdTestService : IOecdTestService
    {
        public int CheckXml(string hash)
        {
            using (var ctx = new OecdTestContext())
            {
                return Convert.ToInt32(ctx.TableWithXmlFields.Any(x => x.md5 == hash));
            }
        }
    }
}
