namespace oecd_test
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TableWithXmlField")]
    public partial class TableWithXmlField
    {
        public int Id { get; set; }

        [Column(TypeName = "xml")]
        [Required]
        public string XmlField { get; set; }

        [Required]
        [StringLength(32)]
        public string md5 { get; set; }
    }
}
