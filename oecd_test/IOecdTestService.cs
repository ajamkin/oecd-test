﻿using System.ServiceModel;

namespace oecd_test
{
    [ServiceContract]
    public interface IOecdTestService
    {
        [OperationContract]
        int CheckXml(string hash);
    }
}
