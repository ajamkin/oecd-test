﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Xml;
using System.Xml.Linq;

namespace oecd_test
{
    class Program
    {
        static void Main(string[] args)
        {
            StartWcfService();

            StoreTestXml();

            ListEstatDatasets();

            Console.ReadLine();
        }


        //1. You need to calculate MD5 hash sum in SQL Server for xml files stored in xml fields of SQL tables.
        //hash is calculated in OecdTestContext.spStoreXml() method
        private static void StoreTestXml()
        {
            var testXml = "<name>Jack</name>";
            using (var ctx = new OecdTestContext())
            {
                var result = ctx.spStoreXml(testXml);
            }
        }


        //2. You will need to read documentation about Eurostat’s Rest SDMX service...
        private static void ListEstatDatasets()
        {
            using (var wc = new WebClient())
            {
                wc.Headers.Add("User-Agent", "Test");
                wc.Headers.Add("Accept", "application/xml");
                var downloadString = wc.DownloadString("http://www.ec.europa.eu/eurostat/SDMX/diss-web/rest/dataflow/ESTAT/all/latest");

                var doc = XDocument.Parse(downloadString);

                var names = doc.Root.Descendants()
                    .Where(x => x.Name.LocalName == "Name"
                                && x.Attributes().Any(y => y.Name.LocalName == "lang" && y.Value == "en")
                    ).Select(x => x.Value);

                foreach (var name in names)
                    Console.WriteLine(name);
            }
        }


        //3. Build a simple webservice that will take as an input md5 hashsum and returns “1” , if xml file...
        private static void StartWcfService()
        {
            var selfHost = new ServiceHost(typeof(OecdTestService));
            try
            {
                selfHost.Open();
                Console.WriteLine("Web service is ready at: {0}", selfHost.BaseAddresses.FirstOrDefault());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error while trying to start web service: {0}", ex.Message);
                selfHost.Abort();
            }
        }


        //4. Suggest workflow for given scenario: You need regularly process large xml/csv/excel files and...
        //the code here is just for illustration
        private static void WorkflowSuggest()
        {
            //Avoid loading whole XML file into memory
            using (var reader = XmlReader.Create("xmlfile"))
            {
                var _batch = new List<TableWithXmlField>();
                while (reader.Read())
                {
                    //collect data from xml, and save it to _batch list
                    _batch.Add(new TableWithXmlField());

                    //when the _batch collection have enough data(for example 100 rows), insert all records at once, save changes to database, and dispose context
                    if (_batch.Count >= 100)
                    {
                        using (var ctx = new OecdTestContext())
                        {
                            //in case of entity framework (or other orm's) disable entity tracking
                            ctx.Configuration.AutoDetectChangesEnabled = false;
                            ctx.TableWithXmlFields.AddRange(_batch);
                            ctx.SaveChanges();
                            _batch.Clear();
                        }
                    }
                }

                //save _batch if there is anything after while cycle is done 
            }

            //done :)
        }

    }
}
